module.exports = {
  parser: '@typescript-eslint/parser',
  settings: {
    react: {
      version: 'detect'
    }
  },
  env: {
    browser: true,
    es2021: true
  },
  extends: ['eslint:recommended', 'standard', 'plugin:react/recommended', 'plugin:react/jsx-runtime'],
  parserOptions: {
    sourceType: 'module'
  },
  rules: {
    indent: ['error', 2],
    quotes: ['error', 'single'],
    'jsx-quotes': ['error', 'prefer-single']
  }
}
