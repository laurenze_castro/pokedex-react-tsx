# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2023-03-11)


### Features

* added readme ([e51f96f](https://gitlab.com/laurenze_castro/pokedex-react-tsx/commit/e51f96f5007cd0f567f1175518c0da773d0fa38d))
