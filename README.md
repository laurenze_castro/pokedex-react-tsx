# My React.JS Pokedex! :video_game:

## Technologies used
- Commitlint
- Husky
- Eslint (JS and TS)
- Standard Version
- and ofc, React :heart_eyes:

## Technical navigation
### Installation procedure (via NPM)
`npm install`

### Start project
`npm run dev` or `npm start` followed by `o` in the console to **open** the app in the browser

### Build project for dist
`npm run build`

### Preview project build locally
`npm run preview`
