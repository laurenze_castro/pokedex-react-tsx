import React from 'react'
import ReactDOM from 'react-dom/client'
import {
  createHashRouter,
  RouterProvider
} from 'react-router-dom'

import './index.css'
import App from './App'
import SinglePokemon from './component/SinglePokemon'

const router = createHashRouter([
  {
    path: '/',
    element: <App/>
  },
  {
    path: '/pokemon/:name/:id',
    element: <SinglePokemon/>
  }
])

ReactDOM.createRoot(document.getElementById('root')as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
)
