type URL_type = {
  [key:string]: (pokemonId: string) => string
}

export const URL:URL_type = {
  getPokemonSprite: (pokemonId) => `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemonId}.png`
}
