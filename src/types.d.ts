export type PokemonResultType = {
  name: string,
  url: string
}

export type PaginationType = {
  offset: number,
  limit: number,
  selectedPage: number,
  pagesToShow: number
}

export type PaginationPropType = {
  count : number,
  setSelectedPage: Function,
  pagination: PaginationType,
  setInputPageCount: Function,
  inputPageCount: number
}

export type SinglePokemonType = {
  sprites: {
    front_default: string,
    other: {
      'official-artwork': {
        front_default: 'string'
      }
    }
  }
}

export type Pokemon = PokemonResultType[]
