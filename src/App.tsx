import { useEffect, useState } from 'react'
import { Pokedex } from 'pokeapi-js-wrapper'

import './App.css'
import { PaginationType, Pokemon } from './types'
import GridCard from './component/GridCard'
import Pagination from './component/Pagination'
import Loader from './component/UI/Loader'

const pokedex = new Pokedex()

function App () {
  const [pokemon, setPokemon] = useState<Pokemon>([])
  const [pagination, setPagination] = useState<PaginationType>({
    offset: 0,
    limit: 12,
    selectedPage: 0,
    pagesToShow: 10
  })
  const [count, setCount] = useState<number>(0)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [inputPageCount, setInputPageCount] = useState<number>(0)

  const fetchData = async () => {
    const { offset, limit } = pagination
    setIsLoading(true)

    try {
      const { results, count } = await pokedex.getPokemonsList({
        offset,
        limit
      })

      setPokemon(results)
      setCount(count)
    } catch (error) {
      return error
    } finally {
      setIsLoading(false)
    }
  }

  const setSelectedPage = (pageNumber: number) => {
    setPagination((pagination) => ({
      ...pagination,
      offset: pagination.limit * pageNumber,
      selectedPage: pageNumber
    }))
  }

  useEffect(() => {
    fetchData()
  }, [pagination])

  return (
    <>
      <div className='App'>
        <h1>Pokedex</h1>

        {isLoading
          ? (
            <Loader />
          )
          : (
            <div className='pokemon__container'>
              {!!pokemon.length &&
              pokemon.map((pokemon) => (
                <GridCard key={pokemon.name} pokemon={pokemon} />
              ))}
            </div>
          )}

        {
          !!count && <Pagination
            pagination={pagination}
            setSelectedPage={setSelectedPage}
            count={count}
            setInputPageCount={setInputPageCount}
            inputPageCount={inputPageCount}
          />
        }

      </div>
    </>
  )
}

export default App
