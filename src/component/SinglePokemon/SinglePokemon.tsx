import { Link, useParams } from 'react-router-dom'
import { useState, useEffect } from 'react'
import { Pokedex } from 'pokeapi-js-wrapper'

import Loader from '../UI/Loader'
import { SinglePokemonType } from '../../types'

const pokedex = new Pokedex()

function SinglePokemon () {
  const { name, id } = useParams()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [singlePokemon, setSinglePokemon] = useState<SinglePokemonType>()

  const fetchData = async () => {
    setIsLoading(true)

    try {
      const result = await pokedex.getPokemonByName(name)
      setSinglePokemon(result)
    } catch (error) {
      return error
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    fetchData()
  }, [name])

  return (
    <>
      <h1>
        <Link to={'/'}>Pokedex</Link>
      </h1>
      {isLoading
        ? (
          <Loader />
        )
        : (
          <h1>
             Pokemon {name} {id}
            <img src={singlePokemon?.sprites.other['official-artwork'].front_default}></img>
          </h1>

        )}
    </>
  )
}

export default SinglePokemon
