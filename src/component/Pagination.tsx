import styles from './Pagination.module.css'

import { PaginationPropType } from '../types'

const spawnButtons = (start: number, end: number): number[] => {
  return new Array(end - start).fill(1).map((_, i) => i + start)
}

function Pagination ({ count, setSelectedPage, pagination, setInputPageCount, inputPageCount }: PaginationPropType) {
  const { pagesToShow, selectedPage } = pagination

  const TOTAL_PAGE_COUNT: number = Math.round(count / 12)
  const START_AND_PREVIOUS_PAGE: number = selectedPage - 1
  const END_PAGE: number = selectedPage + pagesToShow

  const getDisplayedButtons = (): number[] => {
    // If the user reaches the TOTAL_PAGE_COUNT, the pagination will not be adjusting anymore
    if (END_PAGE > TOTAL_PAGE_COUNT) { return spawnButtons(TOTAL_PAGE_COUNT - pagesToShow, TOTAL_PAGE_COUNT) }

    return spawnButtons(
      selectedPage > 0 ? START_AND_PREVIOUS_PAGE : selectedPage,
      END_PAGE
    )
  }

  return (
    <div className={styles.pagination}>
      {getDisplayedButtons().map((pageNumber: number) =>
        <button
          className={`${styles.button} ${pageNumber === pagination.selectedPage ? styles.active : ''}`}
          onClick={() => setSelectedPage(pageNumber)} key={pageNumber}>
          {pageNumber + 1}
        </button>)}

      {/* We need to subtract 1 from the page inputCount becase we start at the 0th index as our starting page */}
      <div>
        <input type='number' className='text' value={inputPageCount} onChange={(e) => setInputPageCount(parseInt(e.target.value))} onKeyDown={(e) => {
          if (e.code === 'Enter') setSelectedPage(inputPageCount - 1)
        } }/>
        <button onClick={() => setSelectedPage(inputPageCount - 1)}>&#8250;</button>
      </div>
    </div>
  )
}

export default Pagination
