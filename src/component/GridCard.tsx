import { useNavigate } from 'react-router-dom'
import styles from './GridCard.module.css'
import { PokemonResultType } from '../types'
import { URL } from '../utils/url'

const GridCard = ({ pokemon }: {pokemon: PokemonResultType}) => {
  const navigate = useNavigate()

  if (!pokemon.url) {
    throw new Error('Error: No Pokemon URL in response')
  }
  // Split the pokemon url and get the number located at the second last index of the array
  const getPokemonId:string = (pokemon.url.split('/').slice(-2, -1).pop())!
  const getPokemonName:string = pokemon.name

  const handleClick = () => navigate(`/pokemon/${getPokemonName}/${getPokemonId}`)

  return (
    <div className={styles.grid__card} onClick={handleClick}>
      <img className={styles['grid__card--image']} src={URL.getPokemonSprite(getPokemonId)}></img>

      <h3> {pokemon.name.toUpperCase()}</h3>
    </div>
  )
}

export default GridCard
